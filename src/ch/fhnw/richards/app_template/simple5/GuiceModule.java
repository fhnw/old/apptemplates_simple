package ch.fhnw.richards.app_template.simple5;

import java.util.Locale;

import com.google.inject.AbstractModule;

public class GuiceModule extends AbstractModule {
	@Override
	protected void configure() {
    bind(ServiceLocator.class).to(ServiceLocator_implementation.class);
	}
}
