package ch.fhnw.richards.app_template.simple5;

import java.util.Locale;

public interface ServiceLocator {
	public void setSelectedLocale(Locale selectedLocale);
	public Locale getSelectedLocale();
}
